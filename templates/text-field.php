<?php
/**
 * Text field template.
 *
 * @package MMetabox
 */

namespace MMetabox;

global $post;

$metabox    = $this;
$the_id     = $post->ID;
$meta_id    = $metabox->get_id();
$meta_title = $metabox->get_title();
$meta_value = get_post_meta( $the_id, $meta_id, true );
?>

<section class="MMetabox-metabox">
	<label for="MMetabox-metabox-<?php echo esc_attr( $meta_id ); ?>">
		<?php esc_html_e( 'Update', 'MMetabox-metabox' ); ?>    
		<?php echo esc_attr( $meta_title ); ?>
	</label>
	<input 
		type="text" 
		id="MMetabox-metabox-<?php echo esc_attr( $meta_id ); ?>" 
		name="<?php echo esc_attr( $meta_id ); ?>" 
		value="<?php echo esc_attr( $meta_value ); ?>"
		>
</section>
