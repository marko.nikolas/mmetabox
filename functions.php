<?php
/**
 * Plugin Name: MMetabox
 * Description: Create custom reusable metabox fields.
 * Version:     0.1.0
 * Author:      Marko Nikolaš
 * Author URI:  https://gitlab.com/marko.nikolas
 * Text Domain: mmetabox
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package MMetabox.
 */

namespace MMetabox;

define( 'PLUGIN_DIR', plugin_dir_url( __FILE__ ) );
define( 'PARTIALS_DIR', __DIR__ . '/templates/' );


require_once 'includes/enqueue.php';

/**
 * Include metabox text class.
 */
require_once 'class/class-text-metabox.php';

/**
 * Add text field metabox class.
 */
require_once 'includes/meta.php';
