<?php
/**
 * Abstract metabox class.
 *
 * @package MMetabox
 */

namespace MMetabox;

/**
 * Extendable class for custom metabox fields.
 */
abstract class Metabox {
	/**
	 * Screen context where the meta box should display.
	 *
	 * @var string
	 */
	private $context;

	/**
	 * The ID of the meta box.
	 *
	 * @var string
	 */
	private $id;

	/**
	 * The display priority of the meta box.
	 *
	 * @var string
	 */
	private $priority;

	/**
	 * Screens where this meta box will appear.
	 *
	 * @var string[]
	 */
	private $screens;

	/**
	 * Path to the template used to display the content of the meta box.
	 *
	 * @var string
	 */
	private $template;

	/**
	 * The title of the meta box.
	 *
	 * @var string
	 */
	private $title;

	/**
	 * Constructor.
	 *
	 * @param string   $id metabox id.
	 * @param string   $title metabox title.
	 * @param string   $template metabox template.
	 * @param string   $context context to render in.
	 * @param string   $priority metabox priority.
	 * @param string[] $screens post types to display metabox on.
	 */
	public function __construct( $id, $title, $template, $context = 'advanced', $priority = 'default', $screens = array() ) {
		if ( is_string( $screens ) ) {
			$screens = (array) $screens;
		}

		$this->context  = $context;
		$this->id       = $id;
		$this->priority = $priority;
		$this->screens  = $screens;
		$this->template = rtrim( $template, '/' );
		$this->title    = $title;
	}

	/**
	 * Get the callable that will the content of the meta box.
	 *
	 * @return callable
	 */
	public function get_callback() {
		return array( $this, 'render' );
	}

	/**
	 * Get the screen context where the meta box should display.
	 *
	 * @return string
	 */
	public function get_context() {
		return $this->context;
	}

	/**
	 * Get the ID of the meta box.
	 *
	 * @return string
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Get the template of the meta box.
	 *
	 * @return string
	 */
	public function get_template() {
		return $this->template;
	}

	/**
	 * Get the display priority of the meta box.
	 *
	 * @return string
	 */
	public function get_priority() {
		return $this->priority;
	}

	/**
	 * Get the screen(s) where the meta box will appear.
	 *
	 * @return array|string|WP_Screen
	 */
	public function get_screens() {
		return $this->screens;
	}

	/**
	 * Get the title of the meta box.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->title;
	}

	/**
	 * Render the content of the meta box using a PHP template.
	 *
	 * @param \WP_Post $post - post template.
	 */
	public function render( \WP_Post $post ) {
		if ( ! is_readable( $this->template ) ) {
			return;
		}

		include $this->template;
	}
}
