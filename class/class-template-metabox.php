<?php
/**
 * HTML class generator for metabox.
 *
 * @package MMetabox
 */

namespace MMetabox;

/**
 * Generate HTML based on metabox template.
 */
class Template_Metabox {

	/**
	 * The postmeta ID
	 *
	 * @var string $id - The ID of the postmeta.
	 */
	private $id;

	/**
	 * The postmeta key
	 *
	 * @var string $title - title of postmeta.
	 */
	private $title;

	/**
	 * Get the postmeta ID.
	 *
	 * @return string
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Get the postmeta title.
	 *
	 * @return string
	 */
	public function get_title() {
		return $this->title;
	}

	/**
	 * Constructor.
	 *
	 * @param string $id - The postmeta ID.
	 * @param string $title - The postmeta Title.
	 */
	public function __construct( $id, $title ) {
		$this->id    = $id;
		$this->title = $title;
	}

	/**
	 * Generates the highlighted comment HTML for the given comment.
	 *
	 * @param string $metabox_template - Metabox template name.
	 *
	 * @return string
	 */
	public function generate( $metabox_template ) {
		$template_path = apply_filters( 'mmetabox_template_path', PARTIALS_DIR . $metabox_template . '.php' );

		if ( ! is_readable( $template_path ) ) {
			return sprintf( '<!-- Could not read "%s" file -->', $template_path );
		}

		ob_start();

		include $template_path;

		return ob_get_clean();
	}
}
