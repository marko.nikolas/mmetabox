<?php
/**
 * MMetabox custom text metabox.
 *
 * @package MMetabox
 */

namespace MMetabox;

require_once 'class-metabox.php';
require_once 'class-template-metabox.php';

/**
 * Text field metabox for storing text meta data.
 */
class Text_Metabox extends Metabox {

	/**
	 * Render the content of the meta box using a PHP template.
	 *
	 * @param \WP_Post $post - post template.
	 */
	public function render( \WP_Post $post ) {

		$meta_id          = $this->get_id();
		$meta_title       = $this->get_title();
		$metabox_template = new Template_Metabox( $meta_id, $meta_title );

		$template = $metabox_template->generate( $this->get_template() );

		echo $template; // @phpcs:ignore
	}
}
