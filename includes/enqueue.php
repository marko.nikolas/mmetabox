<?php
/**
 * Enqueue plugin assets.
 *
 * @package MMetabox
 */

namespace MMetabox;

/**
 * Enqueue stylesheets.
 */
function stylesheets_enqueue() {
	wp_enqueue_style( 'MMetabox-metabox', PLUGIN_DIR . 'style.css', '1.0', 'all' );
}

add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\stylesheets_enqueue' );
