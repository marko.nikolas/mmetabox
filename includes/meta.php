<?php
/**
 * Register add & save hooks for meta fields.
 *
 * @package MMetabox
 * @todo Move this functionality into a class.
 */

namespace MMetabox;

/**
 * TODO: Create admin page for setting META_COLLECTION variable.
 */
define(
	'META_COLLECTION',
	array(
		array(
			'id'    => 'article_url',
			'title' => 'Article URL',
		),
		array(
			'id'    => 'location',
			'title' => 'Location',
		),
		array(
			'id'    => 'dosing',
			'title' => 'Dosing Information',
		),
		array(
			'id'    => 'med_interpretation',
			'title' => 'Medical Interpretation',
		),
		array(
			'id'    => 'publication_info',
			'title' => 'Publication Info',
		),
		array(
			'id'    => 'publication_date',
			'title' => 'Publication Date',
		),
		array(
			'id'    => 'medicine',
			'title' => 'Medicine',
		),
		array(
			'id'    => 'misc',
			'title' => 'Misc',
		),
		array(
			'id'    => 'commentary',
			'title' => 'Commentary',
		),
	)
);

/**
 * Add metaboxes to admin.
 *
 * TODO: Configure field settings from admin page.
 */
function add_meta_boxes() {
	global $post;

	foreach ( META_COLLECTION as $key => $value ) {
		$id    = $value['id'];
		$title = $value['title'];

		$metabox = new Text_Metabox( $id, $title, 'text-field', 'side', 'default', array( 'evidence' ) );

		add_meta_box( $metabox->get_id(), $metabox->get_title(), $metabox->get_callback(), $metabox->get_screens(), $metabox->get_context(), $metabox->get_priority() );

	}

}
add_action( 'add_meta_boxes', __NAMESPACE__ . '\add_meta_boxes' );

/**
 * Save metaboxes in the db
 *
 * @param string $post_id - Current post ID.
 */
function save_meta_boxes( $post_id ) {

	global $post;

	$post_id = $post->ID;

	foreach ( META_COLLECTION as $key => $value ) {
		$id              = $value['id'];
		$meta_key_exists = array_key_exists( $id, $_POST ); // @phpcs:ignore WordPress.CSRF.NonceVerification.NoNonceVerification

		if ( $meta_key_exists ) {
			update_post_meta(
				$post_id,
				$id,
				sanitize_text_field( wp_unslash( $_POST[ $id ] ) ) // @phpcs:ignore WordPress.CSRF.NonceVerification.NoNonceVerification
			);
		}
	}
}
add_action( 'save_post', __NAMESPACE__ . '\save_meta_boxes' );
